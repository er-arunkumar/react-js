# React-JS

__Covered Topics__

1. React Introduction
2. React DOM
3. React Setup
4. Components
5. JSX
6. Exports (Default, Named)
7. Functional & Class Components
8. Props
9. useState
10. Passing data to child components
11. Event Handling
12. Forms
13. HTTP Methods
14. API Fetching Data using AXIOS Library
15. useEffect
16. useRef
17. Prop Drilling
18. useContext
19. React Router
20. React Redux
