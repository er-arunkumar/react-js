import image from './logo.svg';
import './App.css';
import First from './components/First';
import Jsxcontent from './components/Jsxcontent';
import ClassComponent from './components/ClassComponent';
import PropsTopic from './components/PropsTopic';
import States from './components/States';
import Navbar from './components/Navbar';
import Event from './components/Event';
import UseEffect from './components/UseEffect';
import ReactUseRef from './components/ReactUseRef';
import ParentProps from './components/ParentProps';
import UseContextComp from './components/useContext-Concept/UseContextComp';

function App() {
  return (
    <>
      <header className="App-header">
        {/* <img src={image} className="App-logo" alt="logo" />
        <h1>Arunkumar</h1>
        {/* <ClassComponent/>
        <Jsxcontent/> */}
        {/* <PropsTopic name="react js"  role="designer"/> */}
        {/* <States/> */}
        {/* <Navbar/> */}
        {/*<Event/> */}
        {/* <UseEffect/> */}
        {/* <ReactUseRef/> */}
        {/* <ParentProps/> */}
        <UseContextComp/>
      </header>
    </>
  );
}

export default App;
