import React from 'react'
import ChildPropsC from './ChildPropsC'
import ChildPropsB from './ChildPropsB'

export default function ChildPropsA({name}) {
  // 
  return (
    <div>ChildPropsA
      <ChildPropsB name={name}/>
      {/*  */}
    </div>
  )
}
