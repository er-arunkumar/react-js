import React from 'react'
import ChildPropsC from './ChildPropsC'

export default function ChildPropsB({name}) {
  return (
    <div>ChildPropsB
      <ChildPropsC name={name}/>
    </div>
  )
}

