import React from 'react'

export default function ChildPropsC({name}) {
    console.log({name});
  return (
    <div>ChildPropsC
        {name}
    </div>
  )
}
