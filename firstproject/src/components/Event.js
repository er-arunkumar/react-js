import React, { useState } from 'react'

export default function Event() {
    const [username, setUsername] = useState('')
    function handleChange(e){
        // console.log(username);
        // console.log(e.target.value);
        let result = e.target.value;
        setUsername(result)
    }
  return (
    <div>
        <h1>{username}</h1>
        <input type="text" placeholder='Enter your name'onChange={handleChange}></input>
        {/* <button>Update Name</button> */}
    </div>
  )
}
