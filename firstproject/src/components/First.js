import Second from "./Second";

import React from 'react'

function FirstComp() {
  return (
    <div>First</div>
  )
}

function First() {
  return (
    <>
     <h1>First Component</h1>
     {/* JSX Explaination */}
     <p>Welcome to react</p>
     {/* 
     const rootEl = document.getElementById("root");
     const paragraphEl = document.createElement('p');
     paragraphEl.innerHTML = 'Welcome to react';
     rootEl.appendChild(paragraphEL);
      */}
     <Second/>
     <FirstComp/>
    </>
  );
}


export default First;