import React, { useState } from 'react';
// import AmazonTv from '../assets/images/amazonTv.svg'

const Navbar = () => {

  // const {name} = props;
  const [userName,setUserName] = useState();

    console.log(userName);
  // array
  // function()

 const menuItems = [
    {label:'Home',link:"#home"},
    {label:'Imported',link:"#imported"},
    {label:'Web Series',link:"#web-series"},
    {label:'Playground',link:"#playground"},
    {label:'Movies',link:"#movies"},
    {label:'Romance',link:"#romance"},
    {label:'Comedy',link:"#comedy"},
  ]
  
  return (
    <nav className="bg-[#1A2224] py-4">
      <div className='container mx-auto flex justify-between  items-start gap-5  text-white font-semibold'>
          <div>
              {/* <img src={AmazonTv} alt="logo" className='w-40 '/> */}
          </div>
          <div className="">
            <div className='flex flex-col md:flex-row gap-7'>
              {menuItems.map((menu, key)=>(<a key={key} href={menu.link}>{menu.label}</a>))} 
            </div>
          </div>
      </div>
      {/* <p className='text-white'>{props.name}</p> */}
      
    </nav>
  );
};

export default Navbar;
