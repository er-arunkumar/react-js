import React from 'react'
import ChildPropsA from './ChildPropsA'
import ChildPropsB from './ChildPropsB'
import ChildPropsC from './ChildPropsC'

export default function ParentProps() {
  return (
    <div>ParentProps
      <ChildPropsA name="Arun"/>
    </div>
  )
}
