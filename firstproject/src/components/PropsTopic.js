import React from 'react'

function PropsTopic(props) {
    console.log(props);
    const {name, age}=props;
  return (
    <div>PropsTopic
        <h1>{name}</h1>
        <p>{age}</p>
    </div>
  )
}

export default PropsTopic