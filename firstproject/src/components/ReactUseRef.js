import React, { useRef, useState, useEffect} from 'react'

export default function ReactUseRef() {
//     const [inputText, setInputText] = useState("");

//     console.log("Component Re-Render");

//     // const variableName = useRef(initialvalue)
//     // {
//             // variableName.current
//     // }

//     const textValue = useRef();

//     useEffect(()=>{
//         textValue.current = inputText;
//         console.log(textValue.current);
        
//     },[inputText])

    

//     const handleUsername = (e) =>{
//         setInputText(e.target.value);
//     }

//     const handleInput = ()=>{
//         console.log(textValue.current);
//         textValue.current
//     }

//   return (
//     <div>
//         <input ref={textValue} type="text" value={inputText} onChange={handleUsername} style={{backgroundColor:'green'}}/>
//         <p>My username is {inputText}</p>
//         <p>My username is {textValue.current}</p>
//         <button onClick={handleInput}>Submit</button>
//     </div>
//   )
const inputElement = useRef();

  const focusInput = () => {
    inputElement.current.focus();
  };

  return (
    <>
      <input type="text" ref={inputElement} />
      <button onClick={focusInput}>Focus Input</button>
    </>
  );
}
