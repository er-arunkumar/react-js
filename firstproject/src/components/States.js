import React, { useState } from 'react';

    const Counter = () => {
      const [count,setCount] = useState(0);
      const increment = () => {
        setCount(count + count);
      };

      const decrement = () => {
        setCount(count - count);
      };

      return (
        <div>
          <h1>Counter</h1>
          <p>Count: {count}</p>
          <button onClick={increment}>Increment</button>
          <button onClick={decrement}>Decrement</button>
        </div>
      );
    };

    export default Counter;
