import React, { useEffect, useState } from 'react'

export default function UseEffect() {
    const [count, setCount] = useState(0);
    const [time, setTime] = useState(0);
    // useEffect
    // ()=>{} - Effect Function
    // [] - Dependencies Array
    // No dependencies
    // console.log("No dependencies")
    // useEffect(()=>{
    //     console.log("No dependencies")
    // });
    // Empty dependencies Array
    // useEffect(()=>{
    //     console.log("Empty dependencies Array")
    // },[]);
    // State dependencies Array
    useEffect(()=>{
        console.log("State dependencies Array")
    },[time]);
  return (
    <div>
        <button onClick={()=>setCount((value)=>value+1)}>Add</button>
        <button onClick={()=>setCount((value)=>value-1)}>Minus</button>
        <button onClick={()=>setTime((value)=>value+1)}>Time</button>
        <>
        <p>Result: {count}</p>
        <p>Time: {time}</p>
        </>
    </div>
  )
}
