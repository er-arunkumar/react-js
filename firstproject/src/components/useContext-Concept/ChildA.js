import React from 'react'
import ChildB from './ChildB'

export default function ChildA(props) {
  // 
  return (
    <div>ChildA
      {/* {props.name} */}
      <ChildB />
      {/*  */}
    </div>
  )
}
