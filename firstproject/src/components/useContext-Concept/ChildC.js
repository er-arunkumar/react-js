import React, { useContext} from 'react'
import { msgContext } from './UseContextComp'

export default function ChildC() {
  const msg = useContext(msgContext);
  console.log(msg);
  return (
    <div>
      ChildC
      <h2>{msg}</h2>
      
    </div>
  )
}
