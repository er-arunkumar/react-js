import React, {createContext, useState}from 'react'
import ChildA from './ChildA'

export const msgContext = createContext();

export default function UseContextComp({children}) {
    const [msg, setMsg] = useState("hello");
    const handleClick=(e)=>{
        console.log(e.target.value);
        setMsg(e.target.value = "world");
        console.log(msg);
    }
  return (
    <msgContext.Provider value={msg}>
    <div>
        <h1 value={msg} onClick={handleClick}>Use Context</h1>
        <ChildA />
    </div>
    </msgContext.Provider>
  )
}
