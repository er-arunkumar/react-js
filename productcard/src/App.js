import React,{useState} from 'react'
import CricketScore from "./components/CricketScore";
import Navbar from "./components/Navbar";
import ProductList from "./components/ProductList";
import Table from "./components/Table";
import tableList from "./components/menuList.json"
import Form from './components/Form';

function App() {
  const [data, setData] = useState(tableList);
  console.log(data);
  const handleDelete = (productKey)=>{
    const updatedValue = data.filter(item => item.productKey !== productKey);
    setData(updatedValue);
  }
  return (
    <>
      <Navbar name="Arun"/>
      {/* <Table menuList={data} handleDelete={handleDelete}/> */}
      {/* <CricketScore/> */}
      {/* <ProductList/> */}
      <Form/>
    </>
  );
}

export default App;
