import React, { useState } from 'react';


const CricketScore = () => {
  const [score, setScore] = useState(0);

  const handleScoreUpdate = (runs) => {
    setScore(score + runs); 
  };
  const handleScoreReset = (score) =>{
    setScore(score = 0);
  }

  return (
    <div className="container mx-auto mt-10 text-center">
      <h1 className="text-3xl font-bold mb-4">Cricket Score App</h1>
      <p className="text-2xl font-bold mb-4">Score: {score}</p>
      <div className="flex flex-col items-center md:flex-row justify-center *:w-40 *:md:w-auto gap-3">
        <button onClick={() => handleScoreUpdate(1)} className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">1 Run</button>
        <button onClick={() => handleScoreUpdate(2)} className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">2 Runs</button>
        <button onClick={() => handleScoreUpdate(3)} className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">3 Runs</button>
        <button onClick={() => handleScoreUpdate(4)} className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">4 Runs</button>
        <button onClick={() => handleScoreUpdate(6)} className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded">6 Runs</button>
        <button onClick={() => handleScoreReset()} className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded">Reset Score</button>
      </div>
    
    </div>
  );
};

export default CricketScore;