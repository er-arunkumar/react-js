import React, { useState } from 'react';

const Form = () => {
    const [user, setUser] = useState({
        name: '',
        mobile: '',
        gender: '',
        message: '',
        isPresent: null
    }
    );
    const [error,setError] = useState('')

    const [dataSet,setDataSet] = useState([]);

    const handleChange = (e) => {
       const{name,value,type,checked} = e.target;
       const checkedValue = type === 'checked' ? checked : value;
       setUser({...user,[name]:checkedValue})
       
       console.log(user);
    };
    // const handleSubmit = (e)=>{ 
    //     e.preventDefault();

    //     setDataSet(prevData =>[...prevData,user]);

    //     console.log(dataSet );

    //     setUser({
    //         name: '',
    //     mobile: '',
    //     gender: '',
    //     message: '',
    //     isPresent: null
    //     })

    // }

    return (
        <>
            <form className='container mx-auto p-5 border border-gray-200 my-20' >
                <h1 className='text-xl font-bold my-5 text-center'>React Form</h1>
                <div className='w-6/12 mx-auto'>
                    <div className='my-2'>
                        <label htmlFor='username' className='text-gray-800 font-medium text-lg'>Name</label>
                        <input type='text' placeholder='Enter your name' value={user.name} id="userName" name="name" className='p-3 border border-gray-300 w-full' onChange={handleChange} required />
                    </div>
                    <div className='my-2'>
                        <label htmlFor='mobile' className='text-gray-800 font-medium text-lg'>Mobile</label>
                        <input type='text' placeholder='Enter your mobile number' value={user.mobile} id="mobile" name="mobile" className='p-3 border border-gray-300 w-full' onChange={handleChange} required />
                    </div>
                    <div className='flex items-center gap-10 my-2'>
                        <div className='flex gap-3 items-center'>
                            <input type='radio' value="Male" id="male" name="gender" checked={user.gender === 'Male'} onChange={handleChange} required />
                            <label htmlFor='male' className='text-gray-800 font-medium text-lg'>Male</label>
                        </div>
                        <div className='flex gap-3 items-center'>
                            <input type='radio' value="Female" id="female" name="gender" checked={user.gender === 'Female'} onChange={handleChange} required />
                            <label htmlFor='female' className='text-gray-800 font-medium text-lg'>Female</label>
                        </div>
                    </div>
                    <div className='my-2'>
                        <label htmlFor='message' className='text-gray-800 font-medium text-lg'>Message</label>
                        <textarea placeholder='Enter your message' value={user.message} id="message" name="message" className='h-40 resize-none p-3 border border-gray-300 w-full' onChange={handleChange}></textarea>
                    </div>
                    <div className='flex items-center gap-10 my-2'>
                        <div className='flex gap-3 items-center'>
                            <input type='checkbox' id="present" name="isPresent" checked={user.isPresent} onChange={handleChange} />
                            <label htmlFor='present' className='text-gray-800 font-medium text-lg'>Present</label>
                        </div>
                    </div>
                    <button type="submit" className='mx-auto p-3 bg-sky-500 text-gray-50 font-bold'>Submit</button>
                </div>
            </form>

            {/* <div>
                {dataSet.map((data,index)=>{
                    <div key={index}>
                    <p>Name: {data.name}</p>
                    <p>Mobile: {data.mobile}</p>
                    <p>Gender: {data.gender}</p>
                    <p>Message: {data.message}</p>
                    <p>Present: {data.isPresent ? 'Yes' : 'No'}</p>
                    </div>
                })}
                
            </div> */}
        </>
    )
}

export default Form;
