import React, { useState } from 'react';
import AmazonTv from '../assets/images/amazonTv.svg'

const Navbar = () => {
  const [openMenu, setOpenMenu] = useState(false);
  console.log(openMenu);
  const [activeMenuItem, setActiveMenuItem] = useState('');
  const menuItems = [
    {label:'Home',link:"#home"},
    {label:'Imported',link:"#imported"},
    {label:'Web Series',link:"#web-series"},
    {label:'Playground',link:"#playground"},
    {label:'Movies',link:"#movies"},
    {label:'Romance',link:"#romance"},
    {label:'Comedy',link:"#comedy"},
  ];
  const toggleMenu=()=>{
    setOpenMenu(!openMenu);
    console.log(openMenu);
  }
  const handleMenuItemClick = (link) => {
    setActiveMenuItem(link);
  };
  return (
    <nav className="bg-[#1A2224] py-4">
      <div className='container mx-auto flex justify-between  items-start gap-5  text-white font-semibold'>
          <div>
              <img src={AmazonTv} alt="logo" className='w-40 '/>
          </div>
          <button className='md:hidden text-white' onClick={toggleMenu}>
            {!openMenu ?(<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" d="M3 6.75A.75.75 0 0 1 3.75 6h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 6.75M3 12a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75A.75.75 0 0 1 3 12m0 5.25a.75.75 0 0 1 .75-.75h16.5a.75.75 0 0 1 0 1.5H3.75a.75.75 0 0 1-.75-.75" clip-rule="evenodd"/></svg>):(<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 36 36"><path fill="currentColor" d="m19.41 18l7.29-7.29a1 1 0 0 0-1.41-1.41L18 16.59l-7.29-7.3A1 1 0 0 0 9.3 10.7l7.29 7.3l-7.3 7.29a1 1 0 1 0 1.41 1.41l7.3-7.29l7.29 7.29a1 1 0 0 0 1.41-1.41Z" class="clr-i-outline clr-i-outline-path-1"/><path fill="none" d="M0 0h36v36H0z"/></svg>)}
          </button>
          <div className={`absolute md:static  mt-12 p-5 md:p-0 w-full md:w-fit bg-[#1A2224] md:mt-0 ${openMenu ? 'block' : 'hidden'} md:flex`}>
            <div className='flex flex-col md:flex-row gap-7'>
                { menuItems.map((menu, index) => (
              <a key={index} href={menu.link}  className={`text-white font-semibold ${activeMenuItem === menu.link ? 'text-rose-600' : 'text-white'}`} onClick={() => handleMenuItemClick(menu.link)}>{menu.label}</a>
              ))}
            </div>
            
          </div>
      </div>
    </nav>
  );
};

export default Navbar;
