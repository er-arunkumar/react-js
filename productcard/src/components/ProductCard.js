import React from 'react'

const ProductCard = ({product})=>{
    console.log(product);
    const {productName, price, stockStatus,category, productImage}  = product;
    return (
        <>
            <div className="bg-white w-full h-fit mx-auto rounded-2xl overflow-hidden shadow-xl p-3 border my-5 border-gray-200">
                <img src={productImage} alt="" className="w-full h-52 object-contain bg-slate-500" />
                <div className="p-4 *:my-1">
                    <h1 className="text-2xl font-semibold">{productName}</h1>
                    <p className="text-white font-semibold text-sm bg-emerald-600 px-3 py-1 rounded-full inline-block ">{category}</p>
                    <p className="font-semibold text-xl">$ {price}</p>
                    <p className={`${stockStatus? 'text-green-600':'text-red-600'} font-semibold`}>{stockStatus? "In Stock" : "Out of Stock"}</p>
                </div>
                <button className="w-full py-2 text-xl font-semibold text-white bg-blue-500 hover:bg-blue-600">Add to cart</button>
            </div>
        </>
    )
}

export default ProductCard;