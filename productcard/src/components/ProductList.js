import React from 'react'
import ProductCard from './ProductCard'
import productData from './productList.json'

export default function ProductList() {
  return (
    <>
      <div className='container mx-auto'>
        <div className='grid grid-cols-1 md:grid-cols-3 xl:grid-cols-4 gap-5 justify-center my-10'>
          {productData.map(product =>
            <div key={product.productId}>
              <ProductCard product={product}/>
            </div>
          )}
        </div>
      </div>

    </>
  )
}
