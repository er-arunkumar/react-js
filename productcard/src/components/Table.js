import React,{useState} from 'react'

export default function Table({menuList, handleDelete}) {
    const [userName,setUserName] = useState("Arun");

    // console.log(userName);
  // array
  // function()
  const handleInput =(e)=>{
    setUserName(e.target.value)
    console.log(userName);
  }
 
  return (
    <>
    <div className='container mx-auto my-10'>
        <h1 className='text-3xl font-semibold my-10 text-gray-800 text-center uppercase'>Cool Drinks List</h1>
        <table className='min-w-full divide-y divide-gray-200'>
        <thead className='bg-gray-50'>
            <tr className='*:px-6 *:py-3 *:text-left *:text-sm *:font-bold *:text-gray-800 *:uppercase'>
                <th>S.No</th>
                <th>Name</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody className='bg-white divide-y divide-gray-200'>
            {menuList.map(item =>(<tr key={item.productKey} className='*:px-6 *:py-3'>
                <td>{item.productKey}</td>
                <td>{item.productName}</td>
                <td>$ {item.productPrice}</td>
                <td><button onClick ={()=>handleDelete(item.productKey)} className='bg-rose-600 hover:bg-rose-700 font-bold px-3 py-2 rounded-lg text-white'>Delete</button></td>
            </tr>))}
            
        </tbody>
    </table>
    <input type='text' value={userName} className='bg-blue-100'  onChange={handleInput}/>
    <p>{userName}</p>
    </div>
    
    </>
  )
}
