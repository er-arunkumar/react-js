import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home } from './Pages/Home';
import { CustomerData } from './Pages/CustomerData';
import { Navbar } from './Component/Navbar';

function App() {
  return (
    <>
    <BrowserRouter>
    <Navbar/>
    <Routes>
      <Route path='/' Component={Home}/>
      <Route path='/custormer-data' Component={CustomerData}/>
    </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;
