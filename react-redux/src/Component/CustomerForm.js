import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { setCustomer } from '../slice/dataSlice';

export const CustomerForm = () => {

    const [formInput, setFormInput] = useState({
        customerId:"",
        name:"",
        city:""
    });
    const [formSubmit, setFormSubmit] = useState(false);
    const dispatch = useDispatch();

    const handleChange = (e)=>{
        const {name, value} = e.target;
        setFormInput((initialValue)=>({
            ...initialValue,
            [name]:value
        }))

    }

    const handleSubmit= (e) =>{
        e.preventDefault();
        setFormSubmit(true);

        const {customerId, name, city} = formInput;
        if(customerId && name && city){
            console.log("Successful");
        }
        else{
            console.log("Failure");
        }

        dispatch(setCustomer(formInput));

    }

    return (
        <div>
            <h1>Customer Form</h1>
            <form onSubmit={handleSubmit}>
                <label className='form-info'>Customer Id</label>
                <input type="text" name="customerId" value={formInput.customerId} onChange={handleChange} className='form-input'/>
                <label className='form-info'>Name</label>
                <input type="text" name="name" value={formInput.name} onChange={handleChange} className='form-input'/>
                <label className='form-info'>City</label>
                <input type="text" name="city" value={formInput.city} onChange={handleChange} className='form-input'/>
                <button className='primary-btn'>Submit</button>
                {formSubmit && formInput.customerId && formInput.name && formInput.city && (<p>Form Submitted.  </p>)}
            </form>
        </div>
    )
}
