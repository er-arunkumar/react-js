import React from 'react'
import { Link } from 'react-router-dom'

export const Navbar = () => {
  return (
    <div className='navbar'>
        <Link to="/" className='nav-link'>Home</Link>
        <Link to="/custormer-data" className='nav-link'>Customer Data</Link>
    </div>
  )
}
