import React from 'react'
import { useSelector } from 'react-redux'

export const CustomerData = () => {
  const customer = useSelector((state) => state.customerStore.customer);
  console.log(customer);
  return (
    <div>
      {customer.map((customer, index) =>{
        return(
          <div key={index}>
            <p>{customer.customerId}</p>
            <p>{customer.name}</p>
            <p>{customer.city}</p>
          </div>
        )
      })}

    </div>
  )
}
