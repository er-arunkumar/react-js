import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    customer:[]
}

export const customerSlice = createSlice({
    name:"customerData",
    initialState,
    reducers:{
        setCustomer:(state, action)=>{
            state.customer = [...state.customer, action.payload];
        }
    }
})
// action
export const { setCustomer } = customerSlice.actions;

export default customerSlice.reducer;