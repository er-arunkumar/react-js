import { configureStore } from "@reduxjs/toolkit";
import customerReducer from "../slice/dataSlice";

const store = configureStore({
    reducer:{
        customerStore: customerReducer,
    }
});

export default store;