import logo from './logo.svg';
import './App.css';
import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import {BrowserRouter,Routes,Route, Link } from 'react-router-dom';
import Service from './components/pages/Service';
import Webdesign from './components/pages/Webdesign';
import Appdevelop from './components/pages/Appdevelop';

function App() {
  return (
    

    <div className="App">
     
      <BrowserRouter>
      <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/about">About</Link></li>
      <li><Link to="/service">Service</Link></li>
      <li><Link to="/service/web-design">Web Design</Link></li>
      <li><Link to="/service/app-design">App Develop</Link></li>
      <li><Link to="/contact">Contact</Link></li>
    </ul>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/about" element={ <About/>}/>
        <Route path="/service">
          <Route path="web-design" element={ <Webdesign/>}/>
          <Route path="app-design" element={ <Appdevelop/>}/>
        </Route>
        <Route path="contact" element={<Contact/>}/>
        
      </Routes>
     </BrowserRouter>
    </div>
  );
}

export default App;
