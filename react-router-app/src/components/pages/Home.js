import React from 'react'
import {NavLink, useNavigate} from 'react-router-dom'


export default function Home() {

    const navigate = useNavigate();

    const handleSubmit= ()=> {
        //vaildate
        navigate("/contact");
    }
  return (
    <div>
        <div>
        <NavLink to="/about" activeClassName="active-state">Click</NavLink>
        <NavLink
  to="/about"
//  style={{ color: 'blue' }}
activeStyle={{ color: 'green' }}
style={({ isActive }) => ({ color: isActive ? 'green' : 'blue' })}
>
  Messages
</NavLink>
        </div>
        <button onClick={handleSubmit}>Submit</button>
        <h1>Home</h1></div>
  )
}
